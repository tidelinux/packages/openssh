# This is the source.sh script. It is executed by BPM in a temporary directory when compiling a source package
# BPM Expects the source code to be extracted into the automatically created 'source' directory which can be accessed using $BPM_SOURCE
# BPM Expects the output files to be present in the automatically created 'output' directory which can be accessed using $BPM_OUTPUT

DOWNLOAD="https://ftp.openbsd.org/pub/OpenBSD/OpenSSH/portable/openssh-${BPM_PKG_VERSION}.tar.gz"
FILENAME="${DOWNLOAD##*/}"

# The prepare function is executed in the root of the temp directory
# This function is used for downloading files and putting them into the correct location
prepare() {
  wget "$DOWNLOAD"
  tar -xvf "$FILENAME" --strip-components=1 -C "$BPM_SOURCE"
}

# The build function is executed in the source directory
# This function is used to compile the source code
build() {
  ./configure --prefix=/usr                                                         \
              --sysconfdir=/etc/ssh                                                 \
              --with-privsep-user=nobody                                            \
              --with-default-path=/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin \
              --with-superuser-path=/usr/sbin:/usr/bin                              \
              --with-pid-dir=/run                                                   \
              --with-pam
  make
}

# The check function is executed in the source directory
# This function is used to run tests to verify the package has been compiled correctly
check() {
  make -j1 tests
}

# The package function is executed in the source directory
# This function is used to move the compiled files into the output directory
package() {
  make DESTDIR="$BPM_OUTPUT" install
  install -v -m755 contrib/ssh-copy-id "$BPM_OUTPUT"/usr/bin
  install -v -m644 contrib/ssh-copy-id.1 "$BPM_OUTPUT"/usr/share/man/man1
  mkdir -p "$BPM_OUTPUT"/etc/pam.d
  cp ../sshd-pam "$BPM_OUTPUT"/etc/pam.d/sshd
  cp ../sshd_config "$BPM_OUTPUT"/etc/ssh/sshd_config
  mkdir -p "$BPM_OUTPUT"/usr/lib/systemd/system/
  cp ../sshd.service "$BPM_OUTPUT"/usr/lib/systemd/system/sshd.service
  cp ../sshd-keygen.service "$BPM_OUTPUT"/usr/lib/systemd/system/sshd-keygen.service
  install -v -m755 -d "$BPM_OUTPUT"/usr/share/doc/openssh
  install -v -m644 INSTALL LICENCE OVERVIEW README* "$BPM_OUTPUT"/usr/share/doc/openssh
  mkdir -p "$BPM_OUTPUT"/usr/share/licenses/openssh
  cp LICENCE "$BPM_OUTPUT"/usr/share/licenses/openssh/LICENSE
}
